define(['app'], function (app) {
  return app.filter('ntpAgreed', function() {
    return function(ntp, pmc) { 
      return ntp ? 'NTP: '+ntp+'. PMC: '+pmc+'. ' : '';
    };
  })
});