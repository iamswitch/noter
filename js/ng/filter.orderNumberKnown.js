define(['app'], function (app) {
  return app.filter('orderNumberKnown', function() {
    return function(orderNumber, orderType) { 
      return orderNumber ? '**DTC ENTITLED**. '+orderType + ' order: '+orderNumber+'. ' : '*non-DTC*. ';
    };
  })
});