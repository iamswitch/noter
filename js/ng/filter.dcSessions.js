define(['app'], function (app) {
  return app.filter('dcSessions', function() {
    return function(dc) { 
      return dc ? 'DellConnect IDs: '+dc+'. ' : '';
    };
  })
});