define(function () {
 	return function ActivityCtrl($scope) {
    $scope.notes = [];
    //defaults
    var noteDefaults = {
      issue: '',
      orderType: 'DTC',
      pmc: 'phone',
      myIndex: ''
    };

    $scope.note = angular.copy(noteDefaults)
   
    $scope.meToggle = function(e) {
      var elem = angular.element(e.srcElement);
      $(elem).parent().next().slideToggle('fast');
    }

    $scope.editNote = function(index) {
      $scope.note = $scope.notes[index];
      $scope.note.myIndex = index;
      $('#myTab a:first').tab('show'); // Select last tab
    }

    $scope.update = function(note) {


      $scope.master = angular.copy(note);

      if ($scope.dtcForm.$valid) {

        //editing?
        if (angular.isNumber($scope.note.myIndex)) {
          $('<div class="alert alert-success">Note edited!</div>').prependTo($('#dtc-form')).delay(1600).slideToggle('slow', function(){
            $(this).remove();
          });
          $scope.notes[$scope.note.myIndex] = $scope.note;
          $scope.reset();

        } else { //new

          $('<div class="alert alert-success">Note added!</div>').prependTo($('#dtc-form')).delay(1600).slideToggle('slow', function(){
            $(this).remove()
          });
          $scope.notes.push(note);
          $scope.reset();
        }
      }
      else {
        //alert('note not added!');
        $('<div class="alert alert-danger">Note not added!</div>').prependTo($('#dtc-form')).delay(1600).slideToggle('slow', function(){
          $(this).remove();
        });
      }     
    };
   
    $scope.reset = function() {
      $scope.note = angular.copy(noteDefaults)
    };

    $scope.isUnchanged = function(note) {
      return angular.equals(note, $scope.master);
    };
  }
});