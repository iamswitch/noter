define(['app'], function (app) {
  return app.filter('vaDone', function() {
    return function(va) { 
      return va ? '**VA done**.' : '';
    };
  })
});