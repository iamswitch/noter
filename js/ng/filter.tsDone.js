define(['app'], function (app) {
  return app.filter('tsDone', function() {
    return function(ts) { 
      return ts ? '*'+ts+'*' : '';
    };
  })
});