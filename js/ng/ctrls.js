'use strict';
define([
  'app', 
  'ng/ctrl.Activity'], 
  function (app, ActivityCtrl) {
  return app.controller('ActivityCtrl', ActivityCtrl);
});