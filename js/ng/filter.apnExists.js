define(['app'], function (app) {
  return app.filter('apnExists', function() {
    return function(apn) { 
      return apn ? 'APN = '+apn+'. ' : '';
    };
  })
});