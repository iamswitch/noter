define(['app'], function (app) {
  return app.filter('hfnUsed', function() {
    return function(hfn) { 
      return hfn ? 'HFN: Yes. ' : '';
    };
  })
});