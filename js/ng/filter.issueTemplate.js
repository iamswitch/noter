define(['app'], function (app) {
  return app.filter('issueTemplate', function() {
    return function(issue) { 
      return issue ? 'ISSUE: '+issue : '';
    };
  })
});