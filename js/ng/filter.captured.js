define(['app'], function (app) {
  return app.filter('captured', function() {
    return function(contact) { 
      return contact ? 'verified primary phone and email. ' : '';
    };
  })
});