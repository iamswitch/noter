'use strict';
define([
	'app', 
	'ng/filter.serviceTagKnown', 
	'ng/filter.orderNumberKnown',
	'ng/filter.vaDone',
	'ng/filter.captured',
	'ng/filter.apnExists',
	'ng/filter.issueTemplate',
	'ng/filter.kcsUsed',
	'ng/filter.tsDone',
	'ng/filter.hfnUsed',
	'ng/filter.dcSessions',
	'ng/filter.ntpAgreed'
], function (app) {
	return app;
});