define(['app'], function (app) {
  return app.filter('serviceTagKnown', function() {
    return function(serviceTag) { 
      return serviceTag ? 'tag: '+serviceTag+'. ' : '';
    };
  })
});