define(['app'], function (app) {
  return app.filter('kcsUsed', function() {
    return function(kcs) { 
      return kcs ? 'ptr/kcs: '+kcs+'. ' : '';
    };
  })
});