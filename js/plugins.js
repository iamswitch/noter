//plugins

$(function() {
	//tabs
	$('#myTab a').on('click', function(){
		$(this).tab('show')
	})
	//$('#myTab a:last').tab('show') // Select last tab
})

//time
$('#manila-time').text(moment().zone("+08:00").format("M/DD, h:mma"));
$('#eastern-time').text(moment().zone("-04:00").format("M/DD, h:mma"));
$('#central-time').text(moment().zone("-05:00").format("M/DD, h:mma"));
$('#mountain-time').text(moment().zone("-06:00").format("M/DD, h:mma"));
$('#pacific-time').text(moment().zone("-07:00").format("M/DD, h:mma"));

var inputTimeToBeConverted = $('<input id="myDesiredTime" type="text" />');
var desiredMoment;

inputTimeToBeConverted.on('change', function(){
  desiredMoment = moment($(this).val() + ' ' + $(this).attr('title'), "M/DD, h:mma Z");
  $('.timespan').each(function(i, em){
    $(em).text(desiredMoment.zone($(em).attr('data-tz')).format("M/DD, h:mma"));
  })
  //auto set NTP
  var scope = angular.element($("#wrap")).scope();
  scope.$apply(function(){
      var plus2 = moment($('#manila-time').text() + ' +08:00', "M/DD, h:mma Z").add('h', 2).zone("+08:00").format("h:mma");
      scope.note.ntp = $('#manila-time').text() + '-'+plus2+' MNL';
  })

})

$('.timespan').on('click', function(){
  var current_timespan = $(this);
  var current_tz = current_timespan.attr('data-tz');
  $(this).after(inputTimeToBeConverted.attr('title', current_tz).val(current_timespan.text()));
})

//get tz's current time
$('.now.btn').on('click', function(){
  $(this).prev().text(moment().zone($(this).prev().attr('data-tz')).format("M/DD, h:mma"));
})



//popovers
$('#textareaTs').popover({
  trigger: 'hover',
  content: function(){return $('#myCompleteNote').text()},
  placement: 'top'
})

$('#textareaTs').on('click', function(){
  $('.popover-content').html($('#myCompleteNote').text());
  //alert('hello');
})

