require.config({
  baseUrl: 'js',
  paths: {
    //"angular" : "lib/angular/angular"
    //'angular-loader': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular-loader',
    'angular' : 'https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular'
  },
  shim: {
    angular: {exports: 'angular'},
  }
});

require(['app', 'ng/filters', 'ng/ctrls'], function (app, filters, ctrls) {
  "use strict";
  return angular.bootstrap(document, ['Noter']);
});

