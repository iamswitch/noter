define(['angular'], function (angular) {
  'use strict'
  //setup the application
  return angular.module('Noter', []);
});