#Usage Guidelines

1. Issue field is always required.
2. Div at bottom right corner contains all texts from the app.
3. In Logs, all submitted activities are available.
	a. Click on index badge(green if dtc order was provided, red otherwise) to edit a submitted activity.
	b. Click on one-liner 'issue' label to toggle all text view.
4. Use NTP Manager to work with different timezones.
	a. Click any time text on desired zone to add a desired time.
	b. Edit the input box and click anywhere outside to update all zones.
	c. The NTP field in Log will fetch MNL time.